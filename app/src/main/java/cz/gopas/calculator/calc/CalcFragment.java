package cz.gopas.calculator.calc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import cz.gopas.calculator.R;
import cz.gopas.calculator.databinding.FragmentCalcBinding;
import cz.gopas.calculator.history.HistoryEntity;
import cz.gopas.calculator.history.HistoryFragment;
import cz.gopas.calculator.history.HistoryViewModel;

public class CalcFragment extends Fragment {

    private static String TAG = CalcFragment.class.getSimpleName();
    private static String ANS_KEY = "ans";

    public CalcFragment() {
        setHasOptionsMenu(true);
    }

    @Nullable
    private FragmentCalcBinding binding = null;

    private SharedPreferences prefs;

    private HistoryViewModel viewModel;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        prefs = PreferenceManager.getDefaultSharedPreferences(requireContext());
        viewModel = new ViewModelProvider(this).get(HistoryViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        binding = FragmentCalcBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.calc.setOnClickListener(v -> calc());
        binding.share.setOnClickListener(v -> share());
        binding.ans.setOnClickListener(v -> binding.b.getEditText().setText(
                String.valueOf(prefs.getFloat(ANS_KEY, 0))
        ));
        binding.mem.setOnClickListener(
                Navigation.createNavigateOnClickListener(
                        CalcFragmentDirections.showHistory()
                )
        );
        NavHostFragment.findNavController(this)
                .getCurrentBackStackEntry()
                .getSavedStateHandle()
                .getLiveData(HistoryFragment.HISTORY_KEY)
                .observe(
                        getViewLifecycleOwner(),
                        v -> binding.a.getEditText().setText(String.valueOf(v))
                );
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return NavigationUI.onNavDestinationSelected(item, NavHostFragment.findNavController(this))
                || super.onOptionsItemSelected(item);
//        switch (item.getItemId()) {
//            case R.id.about:
//                NavHostFragment.findNavController(this).navigate(CalcFragmentDirections.showAbout());
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
    }

    @Override
    public void onDestroyView() {
        binding = null;
        super.onDestroyView();
    }

    private void calc() {
        Log.d(TAG, "Calc!");

        float a, b;
        try {
            a = Float.parseFloat(binding.a.getEditText().getText().toString());
        } catch (NumberFormatException nfe) {
            a = 0;
        }
        try {
            b = Float.parseFloat(binding.b.getEditText().getText().toString());
        } catch (NumberFormatException nfe) {
            b = 0;
        }

        float result = Float.NaN;

        switch (binding.op.getCheckedRadioButtonId()) {
            case R.id.add:
                result = a + b;
                break;
            case R.id.sub:
                result = a - b;
                break;
            case R.id.mul:
                result = a * b;
                break;
            case R.id.div:
                if (b == 0) {
                    NavHostFragment.findNavController(this).navigate(CalcFragmentDirections.zeroDiv());
                } else {
                    result = a / b;
                }
                break;
        }

        binding.result.setText(String.valueOf(result));
        prefs.edit()
                .putFloat(ANS_KEY, result)
                .apply();
        final HistoryEntity historyEntity = new HistoryEntity();
        historyEntity.value = result;
        viewModel.insert(historyEntity);
    }

    private void share() {
        final Intent intent = new Intent(Intent.ACTION_SEND)
                .setType("text/plain")
                .putExtra(
                        Intent.EXTRA_TEXT,
                        getString(
                                R.string.share_text,
                                binding.result.getText()
                        )
                );
        startActivity(Intent.createChooser(intent, getString(R.string.share)));
    }
}
