package cz.gopas.calculator.history;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface HistoryDao {
    @Query("SELECT * FROM history")
    LiveData<List<HistoryEntity>> getAll();

    @Insert
    void insert(HistoryEntity... items);
}
